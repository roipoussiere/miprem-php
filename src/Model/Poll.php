<?php

namespace Miprem\Model;

class Poll extends AbstractModel
{

    const SAMPLE_SUBJECT = ['label' => 'Which movie you want to watch tonight?'];
    const SAMPLE_PROPOSALS = [
        ['label' => 'Forrest Gump'],
        ['label' => 'West Side Story'],
        ['label' => '2001: A Space Odyssey'],
        ['label' => 'It’s a Wonderful Life']
    ];
    const SAMPLE_TALLY = [
        [21, 14, 32, 31, 24, 42, 9],
        [12, 16, 39, 9, 31, 24, 42],
        [42, 9, 21, 14, 32, 31, 24],
        [31, 24, 42, 12, 16, 39, 9]
    ];

    private array $subject;
    private array $proposals;
    private array $tally;
    private array $grades;

    public function __construct(array $subject, array $proposals, array $tally, $grades = null)
    {
        $this->subject = $subject;
        $this->proposals = $proposals;
        $this->tally = $tally;
        $this->grades = Grades::fromBroadGrades($grades, $tally);
    }

    public static function sample() : self
    {
        return new self(self::SAMPLE_SUBJECT, self::SAMPLE_PROPOSALS, self::SAMPLE_TALLY);
    }

    public static function fromArray(array $pollArray) : self
    {
        return new self(
            $pollArray['subject'] ?? self::SAMPLE_SUBJECT,
            $pollArray['proposals'] ?? self::SAMPLE_PROPOSALS,
            $pollArray['tally'] ?? self::SAMPLE_TALLY,
            Grades::fromBroadGrades($pollArray['grades'] ?? null, $pollArray['tally'] ?? self::SAMPLE_TALLY)
        );
    }

    public static function fromYaml(string $pollYaml) : self
    {
        return self::fromArray(yaml_parse($pollYaml) ?? []);
    }

    public static function fromQueryString(string $pollQueryString) : self
    {
        parse_str($pollQueryString ?? '', $query);
        $poll_array = [];

        foreach ($query as $key => $value) {
            switch ($key) {
                case 'subject':
                    // subject=foo
                case 'subject.label':
                    // subject.label=foo
                    $poll_array['subject'] = ['label' => $value];
                    break;
                case 'proposals':
                    // proposals=first choice,second choice
                    $poll_array['proposals'] = array_map(function($prop) {
                        return ['label' => $prop];
                    }, explode('-', $value));
                    break;
                case preg_match('/proposals\.\d+/', $key) ? true : false:
                    // proposals.0=first choice,proposals.2=second choice
                case preg_match('/proposals\.\d+\.label/', $key) ? true : false:
                    // proposals.0.label=first choice,proposals.6.label=second choice
                    if (!array_key_exists('proposals', $poll_array)) {
                        $poll_array['proposals'] = [];
                    }
                    $id = intval(explode('.', $key)[1]);
                    $poll_array['proposals'][$id] = $value;
                    break;
                case 'tally':
                    // tally=1-9_2-8_3-7
                    $poll_array['tally'] = array_map(function($prop_tally) {
                        return explode('-', $prop_tally);
                    }, explode('_', $value));
                    break;
                case preg_match('/tally\.\d+/', $key) ? true : false:
                    // tally.0=1-9&tally.1=2-8&tally.1=3-7
                    if (!array_key_exists('tally', $poll_array)) {
                        $poll_array['tally'] = [];
                    }
                    $id = intval(explode('.', $key)[1]);
                    $poll_array['tally'][$id] = explode('-', $value);
                    break;
                case 'grades':
                    // grades=nope,hmm,yeup
                    $poll_array['grades'] = array_map(function($grade) {
                        return ['label' => $grade];
                    }, explode('-', $value));
                    break;
                case preg_match('/grades\.\d+/', $key) ? true : false:
                    // grades.0=nope,grades.6=yeup
                case preg_match('/grades\.\d+\.label/', $key) ? true : false:
                    // grades.0.label=nope,grades.6.label=yeup
                    if (!array_key_exists('grades', $poll_array)) {
                        $poll_array['grades'] = [];
                    }
                    $id = intval(explode('.', $key)[1]);
                    $poll_array['grades'][$id] = $value;
                    break;
                default:
                    break;
            }
        }

        return self::fromArray($poll_array);
    }

    public function toArray() : array
    {
        return [
            'subject' => $this->subject,
            'proposals' => $this->proposals,
            'tally' => $this->tally,
            'grades' => $this->grades
        ];
    }

    public function toYaml() : string
    {
        return substr(substr(yaml_emit($this->toArray()), 3), 0, -5);
    }

    public function toQueryString() : string
    {
        $poll_array = [];

        if ($this->subject != self::SAMPLE_SUBJECT) {
            $poll_array['subject'] = $this->subject['label'];
        }

        if ($this->proposals != self::SAMPLE_PROPOSALS) {
            $poll_array['proposals'] = join('-', array_map(function($prop) {
                return $prop['label'];
            }, $this->proposals));
        }

        if ($this->tally != self::SAMPLE_TALLY) {
            $poll_array['tally'] = join('_', array_map(function($prop_tally) {
                return join('-', $prop_tally);
            }, $this->tally));
        }

        if ($this->grades != Grades::fromTally($this->tally)) {
            $poll_array['grades'] = join('-', array_map(function($grade) {
                return $grade['label'];
            }, $this->grades));
        }

        return http_build_query($poll_array);
    }

    public function merge(AbstractModel $that) : AbstractModel
    {
        return self::fromArray(array_merge($this->toArray(), $that->toArray()));
    }

    public function render(\Miprem\Renderer\AbstractRenderer $renderer, array $opt = []) : string
    {
        return $renderer->render($this, $opt);
    }

    public function getIdentifier(\Miprem\Renderer\AbstractRenderer $renderer, array $opt = []) : string
    {
        return $renderer->getIdentifier($this, $opt);
    }

    public function save(\Miprem\Renderer\AbstractRenderer $renderer, string $dirPath, array $opt = []) : string
    {
        return $renderer->save($this, $dirPath, $opt);
    }

    public function setSubject(array $subject) : self
    {
        $this->subject = $subject;
        return self;
    }

    public function setProposals(array $proposals) : self
    {
        $this->proposals = $proposals;
        return self;
    }

    public function setTally(array $tally) : self
    {
        $this->tally = $tally;
        return self;
    }

    public function setGrades(array $grades) : self
    {
        $this->grades = $grades;
        return self;
    }

    public function getSubject() : array
    {
        return $this->subject;
    }

    public function getProposals() : array
    {
        return $this->proposals;
    }

    public function getTally() : array
    {
        return $this->tally;
    }

    public function getGrades() : array
    {
        return $this->grades;
    }

}
