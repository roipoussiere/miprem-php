#!/usr/bin/env bash

# Mount a templates folder from the templates dir in miprem submodule.
# Must be run as a super user.
# sudo umount <root template dir> to unmount.

set -e
ROOT_TEMPLATES_DIR='./templates'
SUBMODULE_TEMPLATES_DIR='miprem/templates'

if [ -d "${ROOT_TEMPLATES_DIR}" ]; then
    if [ "$(ls -A ${ROOT_TEMPLATES_DIR})" ]; then
        echo "Folder ${ROOT_TEMPLATES_DIR} already exist and is not empty.";
        exit 1
    fi
else
    echo "Creating empty dir in ${ROOT_TEMPLATES_DIR}..."
    mkdir ${ROOT_TEMPLATES_DIR}
fi

echo "Mouting ${SUBMODULE_TEMPLATES_DIR} in ${ROOT_TEMPLATES_DIR}..."
mount --bind ${SUBMODULE_TEMPLATES_DIR} ${ROOT_TEMPLATES_DIR}

echo "Done."
